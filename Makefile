CC=gcc
AR=ar
PROG=test
CFLAGS=-Wall -g

all: $(PROG)

$(PROG): test.o list.a
	$(CC) $(CFLAGS) -o $(PROG) $^

%.a: %.o
	$(AR) -rcs $@ $^

%.o: %.c
	$(CC) $(CFLAGS) -c $<

clean:
	rm -f *.o *.a $(PROG)
