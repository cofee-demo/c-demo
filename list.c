#include "list.h"
#include <stdlib.h>

dll_t *dll_create() {
  dll_t *list = (dll_t *)calloc(1, sizeof(dll_t));
  /*
  if (list == NULL) {
        return NULL;
  }
  */
  list->head = NULL;
  list->tail = NULL;
  list->size = 0;
  return list;
}

int dll_insert(dll_t *list, int value, size_t position) {
  if (list == NULL) {
    return -1;
  }
  if (position > list->size || position < 0) {
    return -2;
  }
  list_node_t *current = list->head;
  while (position > 0) {
    current = current->next;
    position--;
  }
  list_node_t *new_node = (list_node_t *)calloc(1, sizeof(list_node_t));
  if (new_node == NULL) {
    return -3;
  }
  new_node->value = value;
  new_node->prev = current;

  // setting next pointer
  if (current != NULL) {
    new_node->next = current->next;
  } else {
    new_node->next = NULL;
    new_node->prev = list->tail;
  }
  // updating previous element
  if (new_node->prev != NULL) {
    new_node->prev->next = new_node;
  } else {
    list->head = new_node;
  }
  // updating next element
  if (new_node->next != NULL) {
    new_node->next->prev = new_node;
  } else {
    list->tail = new_node;
  }
  list->size++;
  return 0;
}

int dll_get(dll_t *list, int *result, size_t position) {

  if (list == NULL) {
    return -1;
  }
  if (position >= list->size || position < 0) {
    return -2;
  }
  list_node_t *current = list->head;
  while (position > 0) {
    current = current->next;
    position--;
  }
  *result = current->value;
  return 0;
}

int dll_remove(dll_t *list, size_t position) {
  if (list == NULL) {
    return -1;
  }
  if (position >= list->size || position < 0) {
    return -2;
  }
  list_node_t *current = list->head;
  while (position > 0) {
    current = current->next;
    position--;
  }
  // updating previous element
  if (current->prev == NULL) {
    list->head = current->next;
  } else {
    current->prev->next = current->next;
  }
  // updating next element
  if (current->next == NULL) {
    list->tail = current->prev;
  } else {
    current->next->prev = current->prev;
  }
  return 0;
}

void dll_delete(dll_t *list) {

  if (list == NULL) {
    return;
  }
  list_node_t *current = list->head;
  while (current != NULL) {
    list_node_t *next = current->next;
    free(current);
    current = next;
  }
  free(list);
}
