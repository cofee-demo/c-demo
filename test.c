#include "list.h"
#include <stdio.h>
#include <stdlib.h>

int main() {
  dll_t *list = dll_create();
  for (int i = 0; i < 10; i++) {
    if (dll_insert(list, i + 1, i)) {
      printf("error inserting element\n");
      exit(1);
    }
  }
  int value;
  if (dll_get(list, &value, 1)) {
    printf("error getting element\n");
    exit(1);
  }
  printf("lists second element: %d\n", value);
  dll_remove(list, 1);
  dll_delete(list);
}
