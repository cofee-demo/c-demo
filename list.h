#ifndef LIST_H
#define LIST_H

#include <stddef.h>

typedef struct list_node {
  struct list_node *next;
  struct list_node *prev;
  int value;
} list_node_t;

typedef struct list {
  list_node_t *head;
  list_node_t *tail;
  size_t size;
} dll_t;

dll_t *dll_create();
int dll_insert(dll_t *list, int value, size_t position);
int dll_get(dll_t *list, int *result, size_t position);
int dll_remove(dll_t *list, size_t position);
void dll_delete(dll_t *list);

#endif
