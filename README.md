# Demo C

This is a CoFee demo with a simple C task.

The task is to implement a doubly linked list.

## Task

Implement a static doubly linked list library list.c in C. 
Your implementation should provide the following functions:

- dll_t* dll_create()
- int dll_insert(dll_t* list, int value, size_t position)
- int dll_get(dll_t* list, int* result, size_t position)
- int dll_remove(dll_t* list, size_t position)
- void dll_delete(dll_t* list)

You should also implement a simple program test.c, 
that is using these methods.

## Errors

- missing error handling for malloc in dll_create
- missing free in dll_remove

## CI File 

The pipline is defined by the ci-file in another repository:

    https://gitlab.com/cofee-demo/cofee_up-ci-files

It is set in the Settings under CI/CD -> General Pipelines -> CI/CD configuration file

To set this in each Students repository, the students eather need to clone a template, where this is set or if the sudents create new repository this can be set using the gitlab api.

## Testing Files

All files for testing are included in the CoFee image that is created here:

    https://gitlab.com/schrc3b6/cofee_up

The actual testing files are included in a submodule that can be found here:

    https://gitlab.com/schrc3b6/cofee_up-test-exercises

